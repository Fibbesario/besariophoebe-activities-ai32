import Vue from 'vue'
import { BootstrapVue } from 'bootstrap-vue'
import App from './App.vue'
import toastr from "toastr";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "toastr/build/toastr.css";
import "@/assets/css/styles.css";
import store from "./store";

//VUE ROUTER
import VueRouter from 'vue-router';
Vue.use(VueRouter)

const routes = [
 {path: '/', component: index},
 {path: '/patronmanagement', component: patronmanagement},
 {path: '/bookmanagement', component: bookmanagement},
 {path: '/settings', component: settings},
];

const router = new VueRouter({
  routes,
  mode: 'history'
});

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue);

Vue.use(
  (toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: "toast-top-right",
    preventDuplicates: false,
    onclick: null,
    showDuration: "300",
    hideDuration: "1000",
    timeOut: "5000",
    extendedTimeOut: "1000",
    showEasing: "swing",
    hideEasing: "linear",
    showMethod: "fadeIn",
    hideMethod: "fadeOut",
  })
);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router,
}).$mount('#app');
