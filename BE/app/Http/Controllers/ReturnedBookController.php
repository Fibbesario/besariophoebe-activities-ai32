<?php

namespace App\Http\Controllers;
use App\Models\Book;
use App\Models\BorrowedBook;
use App\Models\ReturnedBook;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReturnedBookRequest;

class ReturnedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(ReturnedBook::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReturnedBookRequest $request)
    {
        //
        ReturnedBook::create($request->validated());
        $book = Book::find($request->book_id);
        Book::where('id', $request->book_id)->update(['copies' => $book->copies + $request->copies]);
        ReturnedBook::create($request->all());
        $this->destroy($request->patron_id);
        return response()->json(['message' => 'Returned']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        BorrowedBook::where('patron_id', $id)->delete();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
