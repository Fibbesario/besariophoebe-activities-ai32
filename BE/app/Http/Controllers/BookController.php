<?php

namespace App\Http\Controllers;
use App\Models\Book;
use App\Http\Controllers\BookController;
use App\Http\Requests\BookRequest;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       return response()->json(Book::all());
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        //
        Book::create($request->validated());
        return response()->json(['message' => 'Book added.', 'book' => $book->with(['category:id,category'])->where('id', $book->id)->firstOrFail()], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response()->json(Book::with(['category:id,category'])->where('id', $id)->firstOrFail());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, $id)
    {
        //
        Book::where('id', $id)->update($request->all());

        $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
              $book->update($request->validated());
                return response()->json(['message' => 'Book has been updated.', 'book' => $book->with(['category:id,category'])->where('id', $book->id)->firstOrFail()]);
             }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Book::where('id', $id)->delete();
        return response()->json(['message' => 'Deleted']);
    }
}
