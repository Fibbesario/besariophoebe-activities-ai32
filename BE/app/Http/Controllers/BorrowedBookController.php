<?php

namespace App\Http\Controllers;
use App\Models\BorrowedBook;
use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Http\Requests\BorrowedBookRequest;

class BorrowedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(BorrowedBook::with(['patron', 'book', 'book.category'])->get());
       //return ('HATOK');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BorrowedBookRequest $request)
    {
        //
        return response()->json(['message' => 'Book has been borrowed.'], 201);
       $create_borrowed_book = BorrowedBook::create($request->only(['book_id', 'copies', 'patron_id']));
       $borrowed_book = BorrowedBook::with(['book'])->find($create_borrowed_book->id);
       $borrowed_book->book->update(['copies' => $borrowed_book->book->copies - $request->copies]);
       return response()->json(['message' => 'Book has been borrowed', 'borrowed' => $borrowed_book]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
