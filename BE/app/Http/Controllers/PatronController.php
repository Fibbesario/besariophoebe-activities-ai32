<?php

namespace App\Http\Controllers;
use App\Models\Patron;
use App\Http\Controllers\Controller;
use App\Http\Requests\PatronRequest;

class PatronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //return ('HATOK');
       // return response()->json(Patron::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatronRequest $request)
    {
        //
        Patron::create($request->validated());
        return response()->json(['message' => 'Patron has been added.', 'patron' => $patron], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $patron->update($request->validated());
        return response()->json(['message' => 'Patron has been updated.', 'patron' => $patron]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatronRequest $request, $id)
    {
        //
        patron::where('id', $id)->update($request->all());
        $patron->delete();
        return response()->json(['message' => 'Updated.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Patron::where('id', $id)->delete();
        return response()->json(['message' => 'deleted']);
    }
}
