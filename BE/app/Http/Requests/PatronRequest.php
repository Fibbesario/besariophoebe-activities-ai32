<?php

namespace App\Http\Requests;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class PatronRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return 
        [
                  'last_name' => 'required|min:2|max:50', 
                   'first_name' => 'required|min:2|max:50',
                   'middle_name' => 'required|min:2|max:50',
                   'email' => 'required|unique:patrons|email'
          ];
    }

    public function message()
    {
        return [        
            'first_name.required' => 'First Name is required.',
            'first_name.min' => 'First Name must have at least minimum of 2 characters',
            'first_name.max' => 'First Name not exceed in 50 characters',
            'middle_name.required' => 'Middle Name is required.',
            'middle_name.min' => 'Middle Name must have at least minimum of 2 characters',
            'middle_name.max' => 'Middle Name not exceed in 50 characters',
            'last_name.required' => 'Last Name is required.',
            'last_name.min' => 'Last Name must have at least minimum of 2 characters',
            'last_name.max' => 'Last Name not exceed in 50 characters',
            'email.required' => 'Last Name is required.',
            'email.required' => 'First Name is required',
            'email.unique' => 'Email already exists',
        ];
    }
    
    //Display error message
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
/*422 means that the data posted is invalid for this request because laravel validate each request before
it passes onto the controller method*/
