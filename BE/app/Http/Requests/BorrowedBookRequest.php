<?php

namespace App\Http\Requests;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class BorrowedBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $book = Book::find(request()->get('book_id'));
                 if(!empty($book)){
                        $copies = $book->copies;
                    }
                    else{
                       $copies = request()->get('copies');
                   }
        return [
            'patron_id' => 'required|',
            'copies.lte' => 'Copies exceed capacity',
             'book_id' => 'required|exists:patrons,id'
        ];
    }
    

    public function inform()
   {
       return [        
            'patron_id.exists' => 'Patron not exist',
            'copies' => ['required',"lte: {$copies}", 'bail', 'gt:0'],
            'book_id.exists' => 'Book not exist'
        ];
    }

    //Display error message
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
/*422 means that the data posted is invalid for this request because laravel validate each request before
it passes onto the controller method*/