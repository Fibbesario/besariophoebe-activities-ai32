<?php

namespace App\Http\Requests;
use App\Models\BorrowedBook;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class ReturnedBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $borrowed = BorrowedBook::where('book_id', request()->get('book_id'))->where('patron_id', request()->get('patron_id'))->first();
          if (!empty($borrowed)) {
               $copies = $borrowed->copies;
           } else {
             $copies = request()->get('copies');
         }
        return [
            'copies.required' => 'Copies is required.',
            'copies.lte' => 'Exceed ',
            'patron_id.exists' => 'Patron not exist'
        ];
    }

    public function message(){
   
        return [        
               'copies.required' => 'Copies is required.',
               'copies.numeric' => 'Copies must be in number',
               'patron_id.exists' => 'Patron doesnt exist'
           ];
    }

    //Display error message
    protected function failedValidation(Validator $validator) {
       throw new HttpResponseException(response()->json($validator->errors(), 422));
       }
}

/*422 means that the data posted is invalid for this request because laravel validate each request before
it passes onto the controller method*/