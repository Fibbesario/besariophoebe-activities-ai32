<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\BorrowedBookController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PatronController;
use App\Http\Controllers\ReturnedBookController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();

}); */

Route::resources([
        'book'=>BookController::class
    ]);

Route::resources([
    'borrowedbooks'=>BorrowedBookController::class
]);

Route::resources([
    'category'=>CategoryController::class
]);

Route::resources([
    'patron'=>PatronController::class
]);

Route::resources([
    'returnedbooks'=>ReturnedBookController::class
]);